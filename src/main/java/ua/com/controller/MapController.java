package ua.com.controller;

import ua.com.controller.Printable;
import ua.com.enumPack.OtherWorld;
import ua.com.enumPack.PlaneRussia;
import ua.com.enumPack.USA;
import ua.com.file.WholePlane;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapController extends WholePlane {

    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Printable> methodsMenu = new LinkedHashMap<>();
    Scanner input = new Scanner(System.in);

    public MapController() {
        menu.put("1", "  1 - Get Usa Flight");
        menu.put("2", "  2 - Get Bidlostan Flight");
        menu.put("3", "  3 - Get Other World flight");
        menu.put("4", "  4 - Whole world");
        menu.put("5", "  5 - Sort flight range");
        menu.put("E", "  E - exit");


        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
//        methodsMenu.put("5", this::pressButton5);

    }

    private void pressButton2() {
        System.out.println(Arrays.toString(PlaneRussia.values()));
    }

    public void pressButton1() {
        System.out.println(Arrays.toString(USA.values()));
    }

    public void pressButton3() {
        System.out.println(Arrays.toString(OtherWorld.values()));
    }

    public void pressButton4() {
        wholePlane();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}
