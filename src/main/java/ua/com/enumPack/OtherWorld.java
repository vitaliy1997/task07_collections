package ua.com.enumPack;

public enum OtherWorld {

    Euro("Euro Fighter"),China("Hui Chi"), OllWorld("F-16");

    private String destroyer;

    OtherWorld(String destroyer){
        this.destroyer=destroyer;

    }

    @Override
    public String toString() {
        return "OtherWorld{" +
                "destroyer='" + destroyer + '\'' +
                '}';
    }
}
