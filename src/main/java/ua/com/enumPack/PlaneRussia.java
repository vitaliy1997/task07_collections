package ua.com.enumPack;

public enum PlaneRussia {

    Su27("Su-27"),Mig("Mig 29"),SuperJet("SuperJet");

    private String destroyer;

    PlaneRussia(String destroyer){

        this.destroyer=destroyer;
    }

    @Override
    public String toString() {
        return "PlaneRussia{" +
                "destroyer='" + destroyer + '\'' +
                '}';
    }
}
