package ua.com.enumPack;

public enum USA {

    F22("Raptor F-22") ,
    F35("F-35 Lightning II");

    private String destroyer;


    USA(String destroyer){
        this.destroyer=destroyer;
    }

    @Override
    public String toString() {
        return "USA{" +
                "destroyer='" + destroyer + '\'' +
                '}';
    }
}
